module;
// #include <stdio.h>
#include <iostream>

export module hello;

export void greeter(const char *name) {
  // printf("Hello %s!\n", name);
  std::cout << "Hello " << name << std::endl;
}
